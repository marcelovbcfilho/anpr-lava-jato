# import the necessary packages
import numpy as np
from improving_classifier import recognize as plate
from imutils import paths
import argparse
import glob
import cv2
 
# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-a", "--path", required=True,
	help="path to folders images")
ap.add_argument("-p", "--prototxt", required=True,
	help="path to Caffe 'deploy' prototxt file")
ap.add_argument("-m", "--model", required=True,
	help="path to Caffe pre-trained model")
ap.add_argument("-c", "--confidence", type=float, default=0.2,
	help="minimum probability to filter weak detections")
args = vars(ap.parse_args())

# initialize the list of class labels MobileNet SSD was trained to
# detect, then generate a set of bounding box colors for each class
CLASSES = ["background", "aeroplane", "bicycle", "bird", "boat",
	"bottle", "bus", "car", "cat", "chair", "cow", "diningtable",
	"dog", "horse", "motorbike", "person", "pottedplant", "sheep",
	"sofa", "train", "tvmonitor"]
COLORS = np.random.uniform(0, 255, size=(len(CLASSES), 3))

count = 0

# loop over the sample character paths
for samplePath in sorted(glob.glob(args["path"] + "/*")):
	# extract the sample name, grab all images in the sample path, and sample them
	imagePaths = list(paths.list_images(samplePath))

	# loop over all images in the sample path
	for imagePath in imagePaths:
		# load the character, convert it to grayscale, preprocess it, and describe it
		image = cv2.imread(imagePath)

		placa = ""
		(placa, car, count) = plate.detect_character(image, count)
		# Mostra a imagem do Carro
		image_show_text = "Carro com placa: {} Frame: {} ".format(placa, count)

	# show the output image
	#cv2.imshow("Output", image)
	#cv2.waitKey(0)

