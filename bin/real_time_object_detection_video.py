# import the necessary packages
import numpy as np
import recognize as plate
import argparse
import cv2
 
# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--video", required=True,
	help="path to input video")
ap.add_argument("-p", "--prototxt", required=True,
	help="path to Caffe 'deploy' prototxt file")
ap.add_argument("-m", "--model", required=True,
	help="path to Caffe pre-trained model")
ap.add_argument("-c", "--confidence", type=float, default=0.2,
	help="minimum probability to filter weak detections")
args = vars(ap.parse_args())

# initialize the list of class labels MobileNet SSD was trained to
# detect, then generate a set of bounding box colors for each class
CLASSES = ["background", "aeroplane", "bicycle", "bird", "boat",
	"bottle", "bus", "car", "cat", "chair", "cow", "diningtable",
	"dog", "horse", "motorbike", "person", "pottedplant", "sheep",
	"sofa", "train", "tvmonitor"]
COLORS = np.random.uniform(0, 255, size=(len(CLASSES), 3))

# Carrega o vídeo recebido como argumento
video = cv2.VideoCapture(args["video"])

success,image = video.read()
count = 0

#Loop pelos frames do video
while success:
	#Recupera o próximo frame    
	success,image = video.read()
	count += 1

	cv2.imshow("Imagem original", image)
	cv2.waitKey(0)
	# load our serialized model from disk
	net = cv2.dnn.readNetFromCaffe(args["prototxt"], args["model"])

	# load the input image and construct an input blob for the image
	# by resizing to a fixed 300x300 pixels and then normalizing it
	# (note: normalization is done via the authors of the MobileNet SSD
	# implementation)
	(h, w) = image.shape[:2]
	blob = cv2.dnn.blobFromImage(cv2.resize(image, (300, 300)), 0.007843,
		(300, 300), 127.5)

	# pass the blob through the network and obtain the detections and predictions
	net.setInput(blob)
	detections = net.forward()
	# loop over the detections
	for i in np.arange(0, detections.shape[2]):
		# extract the confidence (i.e., probability) associated with the
		# prediction
		confidence = detections[0, 0, i, 2]
	 
		# filter out weak detections by ensuring the `confidence` is
		# greater than the minimum confidence
		if confidence > args["confidence"]:
			# extract the index of the class label from the `detections`,
			# then compute the (x, y)-coordinates of the bounding box for
			# the object
			idx = int(detections[0, 0, i, 1])
			box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
			(startX, startY, endX, endY) = box.astype("int")
			
			 # Código para retirar uma parte da imagem(Placa) e aplicar so filtros apenas na placa
			print(startX, startY, endX, endY)
			placa_teste = image[380:450, 430:520]
			(placa, placa_teste) = plate.detect_character(placa_teste)
			cv2.imshow("Placa", placa_teste)
			print(placa)
			cv2.waitKey(0)
			cv2.imshow("Placa", placa_teste)
			cv2.waitKey(0)
			if CLASSES[idx] == "car":
				placa = ""
				car = image[startY:endY, startX:endX]
				(placa, car) = plate.detect_character(car)
				if(placa != ""):
					# Mostra a imagem do Carro
					image_show_text = "Carro com placa: {} Frame: {} ".format(placa, count)
					cv2.imshow(image_show_text, car)
					cv2.waitKey(0)
					print("Placa do Carro: " + placa)
					# display the prediction
					label = "{}: {:.2f}%".format(CLASSES[idx], confidence * 100)
					# print("[INFO] {}".format(label))
					cv2.rectangle(image, (startX, startY), (endX, endY), COLORS[idx], 2)
					y = startY - 15 if startY - 15 > 15 else startY + 15
					cv2.putText(image, label, (startX, y), cv2.FONT_HERSHEY_SIMPLEX, 0.5, COLORS[idx], 2)

	# show the output image
	#cv2.imshow("Output", image)
	#cv2.waitKey(0)

