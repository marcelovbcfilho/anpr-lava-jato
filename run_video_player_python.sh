# Comando roda automáticamente o vídeo: LavaJato1.mp4 com os devidos  parâmetros
chmod +x run_video_python.sh

current_dir=$(pwd)

python_video_dir=$current_dir'/bin/real_time_object_detection_video_player.py '
python_prototxt_param=$current_dir'/mobile_net/MobileNetSSD_deploy.prototxt.txt'
python_model_param=$current_dir'/mobile_net/MobileNetSSD_deploy.caffemodel'
python_video_param=$current_dir'/examples/placas_teste/LavaJato2.mp4'
python_confidence='0.5'
VAR_TEMP=$python_video_dir' -p '$python_prototxt_param' -m '$python_model_param' -i '$python_video_param' -c '$python_confidence
python3 $VAR_TEMP
