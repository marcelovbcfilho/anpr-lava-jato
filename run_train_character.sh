# Comando roda automáticamente o vídeo: LavaJato1.mp4 com os devidos  parâmetros
chmod +x run_video_python.sh

current_dir=$(pwd)

python_train_dir=$current_dir'/bin/train_advanced.py'
python_samples_param=$current_dir'/character_recognition/examples'
python_char_classifier_param=$current_dir'/character_recognition/adv_char.cpickle'
python_digit_classifier_dir=$current_dir'/character_recognition/adv_digit.cpickle'
VAR_TEMP=$python_train_dir' -s '$python_samples_param' -c '$python_char_classifier_param' -d '$python_digit_classifier_dir
python3 $VAR_TEMP
