# Comando roda automáticamente o vídeo: LavaJato1.mp4 com os devidos  parâmetros
chmod +x run_video_python.sh

current_dir=$(pwd)

python_video_dir=$current_dir'/bin/extract_image_char_training.py '
python_prototxt_param=$current_dir'/mobile_net/MobileNetSSD_deploy.prototxt.txt'
python_model_param=$current_dir'/mobile_net/MobileNetSSD_deploy.caffemodel'
python_image_param=$current_dir'/examples/placas_teste/CarroPrata.png'
python_confidence='0.3'
VAR_TEMP=$python_video_dir' -p '$python_prototxt_param' -m '$python_model_param' -i '$python_image_param' -c '$python_confidence
python3 $VAR_TEMP
